package com.cablemc.pokemoncobbled.forge.mod

import com.cablemc.pokemoncobbled.common.PokemonCobbled
import net.minecraftforge.fml.common.Mod

@Mod(PokemonCobbled.MODID)
class ForgeBootstrap {
    init { PokemonCobbledForge }
}