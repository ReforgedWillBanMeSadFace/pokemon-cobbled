package com.cablemc.pokemoncobbled.common.api.gui

object ColourLibrary {
    const val WHITE = 0xFFFFFF
    const val BUTTON_HOVER_COLOUR = 0xB5C42F
    const val BUTTON_NORMAL_COLOUR = 0xFFFFFF
}