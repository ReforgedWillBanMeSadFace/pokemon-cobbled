package com.cablemc.pokemoncobbled.common.config

annotation class NodeCategory(
    val category: Category
)