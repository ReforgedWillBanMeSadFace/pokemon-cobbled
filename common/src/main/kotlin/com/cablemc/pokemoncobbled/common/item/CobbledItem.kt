package com.cablemc.pokemoncobbled.common.item

import net.minecraft.world.item.Item

/**
 * Base for custom items in Cobbled.
 *
 * Containing common shared code.
 */
open class CobbledItem(
    properties : Properties
) : Item(properties) {

}