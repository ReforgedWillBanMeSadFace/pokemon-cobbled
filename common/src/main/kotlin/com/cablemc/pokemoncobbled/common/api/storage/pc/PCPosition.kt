package com.cablemc.pokemoncobbled.common.api.storage.pc

import com.cablemc.pokemoncobbled.common.api.storage.StorePosition

data class PCPosition(val box: Int, val slot: Int) : StorePosition