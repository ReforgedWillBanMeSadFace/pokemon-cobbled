package com.cablemc.pokemoncobbled.common.api.item

import net.minecraft.resources.ResourceLocation

class Berry(
    val name: ResourceLocation,
    val spicy: Int,
    val dry: Int,
    val sweet: Int,
    val bitter: Int,
    val sour: Int
) {

}