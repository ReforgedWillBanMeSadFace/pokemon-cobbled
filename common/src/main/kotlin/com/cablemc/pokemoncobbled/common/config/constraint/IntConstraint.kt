package com.cablemc.pokemoncobbled.common.config.constraint

annotation class IntConstraint(
    val min : Int,
    val max : Int
)