package com.cablemc.pokemoncobbled.common.pokemon

enum class Gender {
    MALE,
    FEMALE,
    GENDERLESS
}